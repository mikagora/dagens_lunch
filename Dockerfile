FROM debian:jessie 
MAINTAINER Peter

RUN apt-get update && apt-get install -y \ 
bash \ 
less \ 
locales \
wget \
xpdf \
&& rm -r /var/cache

ENV LC_TIME=sv_SE.UTF-8
RUN sed s/'# sv_SE.UTF-8 UTF-8'/'sv_SE.UTF-8 UTF-8'/ /etc/locale.gen > /etc/locale.gen && locale-gen && localedef -i sv_SE -f UTF-8 sv_SE.UTF-8 > /dev/null 2<&1

COPY open.sh /root/open.sh 
RUN chmod +x /root/open.sh

# RUN wget http://www.kvarnen.com/files/dagens_kvarn_v_49_2015.pdf -O lunch.pdf
# RUN lesspipe lunch.pdf
